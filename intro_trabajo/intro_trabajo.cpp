// intro_trabajo.cpp: archivo de proyecto principal.

#include "stdafx.h"
#include <stdio.h>
#include <conio.h>
#include <windows.h>

#define KEY_UP 72
#define KEY_LEFT 75
#define KEY_RIGHT 77
#define KEY_DOWN 80
#define F1 59
#define F2 60
#define F3 61
#define ENTER 13
#define SPACE 32
#define SUPRIM 83
#define TEA 97
#define TEB 98
#define TEP 112
#define TES 115


using namespace System;


void DibujaMarco()
{
	Console::ForegroundColor::set(System::ConsoleColor::White);
	Console::BackgroundColor::set(System::ConsoleColor::Black);
 for(int x=0;x<=79;x++)
 {int y=0;
  Console::SetCursorPosition(x,y);
  if(x==0)
      printf("%c",201);
     if(x==79)
   printf("%c",187);
  if(x==53)
   printf("%c",203);
  else
   printf("%c",205);
 }
 for(int y=1;y<=39;y++)
 {int x=0;
  Console::SetCursorPosition(x,y);
  printf("%c",186);
 }
 for(int y=1;y<=38;y++)
 {int x=79;
  Console::SetCursorPosition(x,y);
  if(y==2)
   printf("%c",185);
  if(y==3)
   printf("%c",185);
  if(y==13)
   printf("%c",185);
  if(y==15)
   printf("%c",185);
  if(y==23)
   printf("%c",185);
  if(y==25)
   printf("%c",185);
  if(y==33)
   printf("%c",185);
  else
      printf("%c",186);
 }
 int h,k;
 h=79;
 k=39;
 Console::SetCursorPosition(h,k);
 printf("%c",188);
 for(int x=0;x<79;x++)
 {int y=39;
  Console::SetCursorPosition(x,y);
  if(x==0)
      printf("%c",200);
     if(x==79)
   printf("%c",188);
  if(x==53)
   printf("%c",202);
  else
   printf("%c",205);
 }
 for(int y=1;y<=38;y++)
 {int x=53;
  Console::SetCursorPosition(x,y);
  if(y==2)
   printf("%c",204);
  if(y==3)
   printf("%c",204);
  if(y==13)
   printf("%c",204);
  if(y==15)
   printf("%c",204);
  if(y==23)
   printf("%c",204);
  if(y==25)
   printf("%c",204);
  if(y==33)
   printf("%c",204);
  else
         printf("%c",186);
 }
 for(int x=54;x<=78;x++)
 {int y=2;
  Console::SetCursorPosition(x,y);
  printf("%c",205);
 }
 for(int x=54;x<=78;x++)
 {int y=3;
  Console::SetCursorPosition(x,y);
  printf("%c",205);
 }
 for(int x=54;x<=78;x++)
 {int y=13;
  Console::SetCursorPosition(x,y);
  printf("%c",205);
 }
 for(int x=54;x<=78;x++)
 {int y=15;
  Console::SetCursorPosition(x,y);
  printf("%c",205);
 }
 for(int x=54;x<=78;x++)
 {int y=23;
  Console::SetCursorPosition(x,y);
  printf("%c",205);
 }
 for(int x=54;x<=78;x++)
 {int y=25;
  Console::SetCursorPosition(x,y);
  printf("%c",205);
 }
 for(int x=54;x<=78;x++)
 {int y=33;
  Console::SetCursorPosition(x,y);
  printf("%c",205);
 }
 Console::SetCursorPosition(0,0);
}
void Cuadrado(int x, int y){
	
	Console::ForegroundColor::set(System::ConsoleColor::DarkRed);
	Console::BackgroundColor::set(System::ConsoleColor::Black);
	Console::SetCursorPosition(x,y);
	printf("%c",177);
	Console::SetCursorPosition(x+1,y);
	printf("%c",177);
	Console::SetCursorPosition(x,y+1);
	printf("%c",177);
	Console::SetCursorPosition(x+1,y+1);
	printf("%c",177);

}
void Cuadrado2(int x, int y){
	
	Console::ForegroundColor::set(System::ConsoleColor::Black);
	Console::BackgroundColor::set(System::ConsoleColor::Black);
	Console::SetCursorPosition(x,y);
	printf("%c",177);
	Console::SetCursorPosition(x+1,y);
	printf("%c",177);
	Console::SetCursorPosition(x,y+1);
	printf("%c",177);
	Console::SetCursorPosition(x+1,y+1);
	printf("%c",177);

}
void Foco(int x,int y){
			Console::BackgroundColor::set(System::ConsoleColor::DarkYellow);
			Console::SetCursorPosition(x,y);
			printf(" ");
			Console::SetCursorPosition(x+1,y);
			printf(" ");
			Console::SetCursorPosition(x,y+1);
			printf(" ");
			Console::SetCursorPosition(x+1,y+1);
			printf(" ");
}
void FocoP(int x,int y){
			Console::BackgroundColor::set(System::ConsoleColor::Yellow);
			Console::SetCursorPosition(x,y);
			printf(" ");
			Console::SetCursorPosition(x+1,y);
			printf(" ");
			Console::SetCursorPosition(x,y+1);
			printf(" ");
			Console::SetCursorPosition(x+1,y+1);
			printf(" ");
}
void Obs(int x,int y){
	Console::BackgroundColor::set(System::ConsoleColor::DarkGreen);
			Console::SetCursorPosition(x,y);
			printf("%c",176);
			Console::SetCursorPosition(x+1,y);
			printf("%c",176);
			Console::SetCursorPosition(x,y+1);
			printf("%c",176);
			Console::SetCursorPosition(x+1,y+1);
			printf("%c",176);
}
void Obs2(int x,int y){
	Console::BackgroundColor::set(System::ConsoleColor::DarkCyan);
			Console::SetCursorPosition(x,y);
			printf("%c",178);
			Console::SetCursorPosition(x+1,y);
			printf("%c",178);
			Console::SetCursorPosition(x,y+1);
			printf("%c",178);
			Console::SetCursorPosition(x+1,y+1);
			printf("%c",178);
}
void Addon(int y){
	switch(y){
		case 1:
			Foco(33,21);
			break;
		case 2:
			Foco(25,11);
			Foco(51,1);
			Obs(23,11);
			for(int i=3;i<=29;i+=2)
				Obs2(41,i);
			break;
	}
}
void Engendro1(int x, int y)
{
    Console::SetCursorPosition(x,y-9);
	Console::ForegroundColor=ConsoleColor::Red;
	printf("      o      \n");
	Console::SetCursorPosition(x,y-8);
	Console::ForegroundColor=ConsoleColor::White;
	printf("      |      \n"); 
	Console::SetCursorPosition(x,y-7);
	Console::ForegroundColor=ConsoleColor::White;
	printf("     [ ]      \n");
	Console::SetCursorPosition(x,y-6);
	Console::ForegroundColor=ConsoleColor::White;
	printf("    (   )      \n");
	Console::SetCursorPosition(x,y-5);
	Console::ForegroundColor=ConsoleColor::White;
	printf("     |>|      \n");
	Console::SetCursorPosition(x,y-4);
	Console::ForegroundColor=ConsoleColor::White;
	printf("  __/===%c__      \n",92);
	Console::SetCursorPosition(x,y-3);
	Console::ForegroundColor=ConsoleColor::White;
	printf(" //| ");
	Console::ForegroundColor=ConsoleColor::Red;
	printf("o");
	Console::ForegroundColor=ConsoleColor::White;
	printf("=");
	Console::ForegroundColor=ConsoleColor::Red;
	printf("o");
	Console::ForegroundColor=ConsoleColor::White;
	printf(" |%c%c      \n",92,92);
	Console::SetCursorPosition(x,y-2);
	Console::ForegroundColor=ConsoleColor::White;
	printf("<  | ");
	Console::ForegroundColor=ConsoleColor::Red;
	printf("o");
	Console::ForegroundColor=ConsoleColor::White;
	printf("=");
	Console::ForegroundColor=ConsoleColor::Red;
	printf("o");
	Console::ForegroundColor=ConsoleColor::White;
	printf(" |  >      \n");
	Console::SetCursorPosition(x,y-1);
	Console::ForegroundColor=ConsoleColor::White;
	printf("   %c=====/      \n",92);
	Console::ForegroundColor=ConsoleColor::White;
	Console::SetCursorPosition(x,y);
	printf("  / /   %c %c      \n",92,92);
}
void Engendro2(int x, int y)
{
	Console::SetCursorPosition(x,y-9);
	Console::ForegroundColor=ConsoleColor::Yellow;
	printf("      o      \n");
	Console::SetCursorPosition(x,y-8);
	Console::ForegroundColor=ConsoleColor::White;
	printf("      |      \n");
	Console::SetCursorPosition(x,y-7);
	Console::ForegroundColor=ConsoleColor::White;
	printf("     [ ]      \n");
	Console::SetCursorPosition(x,y-6);
	Console::ForegroundColor=ConsoleColor::White;
	printf("    (   )      \n");
	Console::SetCursorPosition(x,y-5);
	Console::ForegroundColor=ConsoleColor::White;
	printf("     |>|      \n");
	Console::SetCursorPosition(x,y-4);
	Console::ForegroundColor=ConsoleColor::White;
	printf("  __/===%c__      \n",92);
	Console::SetCursorPosition(x,y-3);
	Console::ForegroundColor=ConsoleColor::White;
	printf(" //| ");
	Console::ForegroundColor=ConsoleColor::Yellow;
	printf("o");
	Console::ForegroundColor=ConsoleColor::White;
	printf("=");
	Console::ForegroundColor=ConsoleColor::Yellow;
	printf("o");
	Console::ForegroundColor=ConsoleColor::White;
	printf(" |%c%c      \n",92,92);
	Console::SetCursorPosition(x,y-2);
	Console::ForegroundColor=ConsoleColor::White;
	printf("<  | ");
	Console::ForegroundColor=ConsoleColor::Yellow;
	printf("o");
	Console::ForegroundColor=ConsoleColor::White;
	printf("=");
	Console::ForegroundColor=ConsoleColor::Yellow;
	printf("o");
	Console::ForegroundColor=ConsoleColor::White;
	printf(" |  >      \n");
	Console::SetCursorPosition(x,y-1);
	Console::ForegroundColor=ConsoleColor::White;
	printf("   %c=====/      \n",92);
	Console::ForegroundColor=ConsoleColor::White;
	Console::SetCursorPosition(x,y);
	printf("  / /   %c %c      \n",92,92);
}
void MataEngendro(int x, int y)
{
	Console::SetCursorPosition(x,y-2);
    Console::ForegroundColor=ConsoleColor::Black;
	printf("**");

}
void Pantalla1()
{
int a,b;
a=60;b=39;
for(int y=0;y<=39;y++)
{int x=7;
 Console::SetCursorPosition(x,y);  
 if(y==16)
 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
}
for(int y=0;y<=39;y++)
{int x=66;
 Console::SetCursorPosition(x,y);
 if(y==10)
 {Console::ForegroundColor=ConsoleColor::Yellow;	
	 printf(":");
 }
 if(y==35)
 {Engendro1(x,y);}
Console::SetCursorPosition(a,b);
Console::ForegroundColor=ConsoleColor::White; 
printf("Presione Enter...");
 }
_sleep(50);
for(int y=0;y<=39;y++)
{int x=8;
 Console::SetCursorPosition(x,y);
 if(y==15)
{Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==16)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");} 
 }
for(int y=0;y<=39;y++)
{int x=65;
 Console::SetCursorPosition(x,y);  
 if(y==10)
 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==35)
 {Engendro2(x,y);}
 Console::SetCursorPosition(a,b);
Console::ForegroundColor=ConsoleColor::White; 
printf("Presione Enter...");

}
  _sleep(50);
for(int y=0;y<=39;y++)
{int x=9;
 Console::SetCursorPosition(x,y);  
 if(y==14)
 {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==15)
	{Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==16)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");} 
}
for(int y=0;y<=39;y++)
{int x=64;
 Console::SetCursorPosition(x,y);  
 if(y==10)
 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
  if(y==35)
 {Engendro1(x,y);}
Console::SetCursorPosition(a,b);
Console::ForegroundColor=ConsoleColor::White; 
printf("Presione Enter...");
}
_sleep(50);
for(int y=0;y<=39;y++)
{int x=10;
 Console::SetCursorPosition(x,y);  
 if(y==13)
	 {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if (y==14)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==15)
	{Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==16)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==20)
	 {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
}
for(int y=0;y<=39;y++)
{int x=63;
 Console::SetCursorPosition(x,y);
 if(y==10)
 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==17)
 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
  if(y==35)
 {Engendro2(x,y);}
Console::SetCursorPosition(a,b);
Console::ForegroundColor=ConsoleColor::White; 
printf("Presione Enter...");
}
_sleep(50);
for(int y=0;y<=39;y++)
{int x=11;
 Console::SetCursorPosition(x,y);  
 if(y==12)
	 {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==13)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==14)
	 {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==16)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==20)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
}
for(int y=0;y<=39;y++)
{int x=62;
 Console::SetCursorPosition(x,y);  
 if(y==10)
 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==11)
 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==17)
 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
  if(y==35)
 {Engendro1(x,y);}
Console::SetCursorPosition(a,b);
Console::ForegroundColor=ConsoleColor::White; 
printf("Presione Enter...");
}
_sleep(50);
for(int y=0;y<=39;y++)
{int x=12;
 Console::SetCursorPosition(x,y);  
 if(y==11)
 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==12)
 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==13)
	{Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==16)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==20)
	 {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
}
for(int y=0;y<=39;y++)
{int x=61;
 Console::SetCursorPosition(x,y);  
 if(y==10)
 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==11)
	 {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==12)
	 {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==17)
 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
  if(y==35)
 {Engendro2(x,y);}
Console::SetCursorPosition(a,b);
Console::ForegroundColor=ConsoleColor::White; 
printf("Presione Enter...");
}
_sleep(50);
for(int y=0;y<=39;y++)
{int x=13;
 Console::SetCursorPosition(x,y);  
 if(y==10)
 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==11)
	{Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==12)
	 {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==16)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==20)
	 {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
}
for(int y=0;y<=39;y++)
{int x=60;
 Console::SetCursorPosition(x,y);
 if(y==10)
 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==11)
 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==12)
 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==13)
	 {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==17)
 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
  if(y==35)
 {Engendro1(x,y);}
Console::SetCursorPosition(a,b);
Console::ForegroundColor=ConsoleColor::White; 
printf("Presione Enter...");
}
_sleep(50);
for(int y=0;y<=39;y++)
{int x=14;
 Console::SetCursorPosition(x,y);  
 if(y==10)
 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==11)
 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==16)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==20)
 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
}
for(int y=0;y<=39;y++)
{int x=59;
 Console::SetCursorPosition(x,y);
 if(y==10)
 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==12)
	 {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==13)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==14)
	 {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==17)
 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
  if(y==35)
 {Engendro2(x,y);}
Console::SetCursorPosition(a,b);
Console::ForegroundColor=ConsoleColor::White; 
printf("Presione Enter...");
}
_sleep(50);
for(int y=0;y<=39;y++)
{int x=15;
 Console::SetCursorPosition(x,y);  
 if(y==10)
 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==16)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==20)
	 {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
}
for(int y=0;y<=39;y++)
{int x=58;
 Console::SetCursorPosition(x,y);
 if(y==10)
 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==13)
	 {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==14)
	{Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==15)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==17)
 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==18)
{Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==35)
 {Engendro1(x,y);}
Console::SetCursorPosition(a,b);
Console::ForegroundColor=ConsoleColor::White; 
printf("Presione Enter...");
}
_sleep(50);
for(int y=0;y<=39;y++)
{int x=16;
 Console::SetCursorPosition(x,y);  
 if(y==16)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==20)
	 {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
}
for(int y=0;y<=39;y++)
{int x=57;
 Console::SetCursorPosition(x,y);
 if(y==10)
 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==14)
{Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==15)
	 {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==16)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==17)
 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==18)
	 {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==19)
 {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==35)
 {Engendro2(x,y);}
 Console::SetCursorPosition(a,b);
Console::ForegroundColor=ConsoleColor::White; 
printf("Presione Enter...");
}
_sleep(50);
for(int y=0;y<=39;y++)
{int x=17;
 Console::SetCursorPosition(x,y);  
 if(y==20)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
}
for(int y=0;y<=39;y++)
{int x=56;
 Console::SetCursorPosition(x,y);
 if(y==15)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==16)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==17)
 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==18)
{Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==19)
	{Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==20)
	 {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
  if(y==35)
 {Engendro1(x,y);}
Console::SetCursorPosition(a,b);
Console::ForegroundColor=ConsoleColor::White; 
printf("Presione Enter...");
}
_sleep(50);
for(int y=0;y<=39;y++)
{int x=18;
 Console::SetCursorPosition(x,y);  
 if(y==16)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==20)
	 {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
}
for(int y=0;y<=39;y++)
{int x=55;
 Console::SetCursorPosition(x,y);
 if(y==10)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==16)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==17)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==19)
	 {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==20)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==21)
	 {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
  if(y==35)
 {Engendro2(x,y);}
Console::SetCursorPosition(a,b);
Console::ForegroundColor=ConsoleColor::White; 
printf("Presione Enter...");
}
_sleep(50);
for(int y=0;y<=39;y++)
{int x=19;
 Console::SetCursorPosition(x,y);  
 if(y==16)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==20)
	 {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
}
for(int y=0;y<=39;y++)
{int x=54;
 Console::SetCursorPosition(x,y);
 if(y==10)
{Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==11)
	{Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==17)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==20)
	 {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==21)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==22)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
  if(y==35)
 {Engendro1(x,y);}
Console::SetCursorPosition(a,b);
Console::ForegroundColor=ConsoleColor::White; 
printf("Presione Enter...");
}
_sleep(50);
for(int y=0;y<=39;y++)
{int x=20;
 Console::SetCursorPosition(x,y);  
 if(y==16)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==20)
	{Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");};
}
for(int y=0;y<=39;y++)
{int x=53;
 Console::SetCursorPosition(x,y);
 if(y==10)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==11)
	 {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==12)
	 {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==17)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==21)
	 {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==22)
	 {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==23)
	{Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
  if(y==35)
 {Engendro2(x,y);}
Console::SetCursorPosition(a,b);
Console::ForegroundColor=ConsoleColor::White; 
printf("Presione Enter...");
}
_sleep(50);
for(int y=0;y<=39;y++)
{int x=21;
 Console::SetCursorPosition(x,y);  
 if(y==16)
	{Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==20)
	 {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
}
for(int y=0;y<=39;y++)
{int x=52;
 Console::SetCursorPosition(x,y);
 if(y==11)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==12)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==13)
	 {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==22)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==23)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
  if(y==35)
 {Engendro1(x,y);}
Console::SetCursorPosition(a,b);
Console::ForegroundColor=ConsoleColor::White; 
printf("Presione Enter...");
}
_sleep(50);
for(int y=0;y<=39;y++)
{int x=22;
 Console::SetCursorPosition(x,y);  
 if(y==16)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==20)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
}
for(int y=0;y<=39;y++)
{int x=51;
 Console::SetCursorPosition(x,y);
 if(y==12)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==13)
	 {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==14)
	 {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==17)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==18)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");} 
 if(y==23)
	{Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
if(y==35)
{MataEngendro(x,y);}
 if(y==35)
 {Engendro2(x+1,y);}

 Console::SetCursorPosition(a,b);
 Console::ForegroundColor=ConsoleColor::White; 
printf("Presione Enter...");
}
_sleep(50);
for(int y=0;y<=39;y++)
{int x=23;
 Console::SetCursorPosition(x,y);  
 if(y==15)
	{Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==16)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
}
for(int y=0;y<=39;y++)
{int x=50;
 Console::SetCursorPosition(x,y);
 if(y==13)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==14)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==15)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==17)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==18)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==19)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
if(y==35)
{MataEngendro(x+1,y);}
 if(y==35)
 {Engendro1(x+3,y);}
Console::SetCursorPosition(a,b);
Console::ForegroundColor=ConsoleColor::White; 
printf("Presione Enter...");
}
_sleep(50);
for(int y=0;y<=39;y++)
{int x=24;
 Console::SetCursorPosition(x,y);  
 if(y==10)
	{Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==14)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==15)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==16)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
}
for(int y=0;y<=39;y++)
{int x=49;
 Console::SetCursorPosition(x,y);
 if(y==13)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==14)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==15)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==16)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==17)
	{Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==18)
	{Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==19)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==20)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==35)
{MataEngendro(x+3,y);} 
 if(y==35)
 {Engendro2(x+5,y);}
Console::SetCursorPosition(a,b);
Console::ForegroundColor=ConsoleColor::White; 
printf("Presione Enter...");
 }
_sleep(50);
for(int y=0;y<=39;y++)
{int x=25;
 Console::SetCursorPosition(x,y);  
 if(y==10)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==13)
	 {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==14)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==15)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==16)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
}
for(int y=0;y<=39;y++)
{int x=48;
 Console::SetCursorPosition(x,y);
 if(y==10)
{Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==13)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==15)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==16)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==17)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==19)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==20)
	{Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==21)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==35)
{MataEngendro(x+5,y);} 
 if(y==35)
 {Engendro1(x+7,y);}
Console::SetCursorPosition(a,b);
Console::ForegroundColor=ConsoleColor::White; 
printf("Presione Enter...");
}
_sleep(50);
for(int y=0;y<=39;y++)
{int x=26;
 Console::SetCursorPosition(x,y);  
 if(y==10)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==12)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==13)
	{Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==14)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==16)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==23)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
}
for(int y=0;y<=39;y++)
{int x=47;
 Console::SetCursorPosition(x,y);
 if(y==10)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==11)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==13)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==16)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==17)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==20)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==21)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==22)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==35)
{MataEngendro(x+7,y);}
  if(y==35)
 {Engendro2(x+9,y);}
Console::SetCursorPosition(a,b);
Console::ForegroundColor=ConsoleColor::White; 
printf("Presione Enter...");
}
_sleep(50);
for(int y=0;y<=39;y++)
{int x=27;
 Console::SetCursorPosition(x,y);  
 if(y==10)
    {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==11)
	{Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==12)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==13)
	 {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==16)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==22)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==23)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
}
for(int y=0;y<=39;y++)
{int x=46;
 Console::SetCursorPosition(x,y);
if(y==10)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==11)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==12)
	 {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==13)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==17)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==21)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==22)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==35)
{MataEngendro(x+9,y);}
  if(y==35)
 {Engendro1(x+11,y);}
Console::SetCursorPosition(a,b);
Console::ForegroundColor=ConsoleColor::White; 
printf("Presione Enter...");
 }
_sleep(50);
for(int y=0;y<=39;y++)
{int x=28;
 Console::SetCursorPosition(x,y);  
 if(y==10)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==11)
	 {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==12)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==16)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==21)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==22)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==23)
	{Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
}
for(int y=0;y<=39;y++)
{int x=45;
 Console::SetCursorPosition(x,y);
 if(y==11)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==12)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==13)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==17)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==22)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==23)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==35)
{MataEngendro(x+11,y);}
  if(y==35)
 {Engendro2(x+13,y);}
Console::SetCursorPosition(a,b);
Console::ForegroundColor=ConsoleColor::White; 
printf("Presione Enter...");
}
_sleep(50);
for(int y=0;y<=39;y++)
{int x=29;
 Console::SetCursorPosition(x,y);  
 if(y==10)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==11)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==20)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==21)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==22)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==23)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
}
for(int y=0;y<=39;y++)
{int x=44;
 Console::SetCursorPosition(x,y);
 if(y==12)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==13)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==14)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==17)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==18)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==23)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==35)
{MataEngendro(x+13,y);}
 if(y==35)
 {Engendro1(x+15,y);}
Console::SetCursorPosition(a,b);
Console::ForegroundColor=ConsoleColor::White; 
printf("Presione Enter...");
 }
_sleep(50);
for(int y=0;y<=39;y++)
{int x=30;
 Console::SetCursorPosition(x,y);  
 if(y==10)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==15)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==16)
	{Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==19)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==20)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==21)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==23)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
}
for(int y=0;y<=39;y++)
{int x=43;
 Console::SetCursorPosition(x,y);
if(y==10)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==11)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==13)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==14)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==15)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==18)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==19)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==23)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==35)
{MataEngendro(x+15,y);}
  if(y==35)
 {Engendro2(x+17,y);}
Console::SetCursorPosition(a,b);
Console::ForegroundColor=ConsoleColor::White; 
printf("Presione Enter...");
}
_sleep(50);
for(int y=0;y<=39;y++)
{int x=31;
 Console::SetCursorPosition(x,y);  
 if(y==10)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==14)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==15)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==16)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==18)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==19)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==20)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==23)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
}
for(int y=0;y<=39;y++)
{int x=42;
 Console::SetCursorPosition(x,y);
 if(y==10)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==11)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==14)
 {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");} 
 if(y==15)
 {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==16)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==18)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==19)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==20)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==23)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==35)
{MataEngendro(x+17,y);}
 if(y==35)
 {Engendro1(x+19,y);}
Console::SetCursorPosition(a,b);
Console::ForegroundColor=ConsoleColor::White; 
printf("Presione Enter...");
 }
_sleep(50);
for(int y=0;y<=39;y++)
{int x=32;
 Console::SetCursorPosition(x,y);  
 if(y==10)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==13)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==14)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==15)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==16)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==17)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==18)
	 {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==19)
	 {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==20)
	 {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==23)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
}
for(int y=0;y<=39;y++)
{int x=41;
 Console::SetCursorPosition(x,y);
if(y==10)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==11)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==15)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==16)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==19)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==20)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==21)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==23)
	{Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==35)
{MataEngendro(x+19,y);}
   if(y==35)
 {Engendro2(x+21,y);}
Console::SetCursorPosition(a,b);
Console::ForegroundColor=ConsoleColor::White; 
printf("Presione Enter...");
}
_sleep(50);
for(int y=0;y<=39;y++)
{int x=33;
 Console::SetCursorPosition(x,y);  
 if(y==10)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
if(y==12)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==13)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==14)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==16)
	{Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==17)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==18)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==20)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==23)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
}
for(int y=0;y<=39;y++)
{int x=40;
 Console::SetCursorPosition(x,y);
if(y==10)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==14)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==16)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==17)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==18)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==20)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==21)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==22)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==23)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==35)
{MataEngendro(x+21,y);}
  if(y==35)
 {Engendro1(x+23,y);}
Console::SetCursorPosition(a,b);
Console::ForegroundColor=ConsoleColor::White; 
printf("Presione Enter...");
}
_sleep(50);
for(int y=0;y<=39;y++)
{int x=34;
 Console::SetCursorPosition(x,y);  
  if(y==10)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==11)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==12)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==13)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==16)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==17)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==20)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==22)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==23)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
}
for(int y=0;y<=39;y++)
{int x=39;
 Console::SetCursorPosition(x,y);
if(y==10)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==14)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==15)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==17)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==18)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==19)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==21)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==22)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==23)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==35)
{MataEngendro(x+23,y);}
  if(y==35)
 {Engendro2(x+25,y);}
Console::SetCursorPosition(a,b);
Console::ForegroundColor=ConsoleColor::White; 
printf("Presione Enter...");
}
_sleep(50);
for(int y=0;y<=39;y++)
{int x=35;
 Console::SetCursorPosition(x,y);  
 if(y==11)
	 {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==12)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==16)
	{Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==17)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==20)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==21)
	{Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==22)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
}
for(int y=0;y<=39;y++)
{int x=38;
 Console::SetCursorPosition(x,y);
if(y==10)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==14)
	{Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==15)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==17)
	{Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==18)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==19)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==22)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==23)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==35)
{MataEngendro(x+25,y);}
  if(y==35)
 {Engendro1(x+27,y);}
Console::SetCursorPosition(a,b);
Console::ForegroundColor=ConsoleColor::White; 
printf("Presione Enter...");
}
_sleep(50);
for(int y=0;y<=39;y++)
{int x=36;
 Console::SetCursorPosition(x,y);  
if(y==10)
	{Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==11)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==16)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==17)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==20)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==21)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==22)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
}
for(int y=0;y<=39;y++)
{int x=37;
 Console::SetCursorPosition(x,y);
 if(y==10)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==14)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==15)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==16)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==17)
	{Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==19)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==20)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==21)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==35)
{MataEngendro(x+27,y);}
  if(y==35)
 {Engendro2(x+29,y);}
Console::SetCursorPosition(a,b);
Console::ForegroundColor=ConsoleColor::White; 
printf("Presione Enter...");
}
_sleep(50);
}

void Pantalla2()
{
int a,b,c,d;
a=60;b=39;c=66;d=35;
for(int x=7;x<=36;x++)
{int y=10;
 Console::SetCursorPosition(x,y);
if(x==7)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");Engendro1(c,d);_sleep(3);}
if(x==8)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==9)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==10)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==11)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==12)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==13)
   {Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==14)
   {Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==15)
   {Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==16)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==17)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==18)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==19)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==20)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==21)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==22)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");Engendro2(c-1,d);_sleep(3);}
if(x==23)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==24)
   {Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==25)
   {Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==26)
   {Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==27)
   {Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==28)
   {Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==29)
   {Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==30)
   {Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==31)
   {Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==32)
   {Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==33)
   {Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==34)
   {Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==35)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==36)
   {Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
}
for(int x=66;x>=37;x--)
{int y=10;
 Console::SetCursorPosition(x,y);
if(x==66)
{Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");Engendro1(c-2,d);_sleep(3);}
if(x==65)
{Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==64)
{Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==63)
{Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==62)
{Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==61)
{Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==60)
{Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==59)
{Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==58)
{Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==57)
{Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==56)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==55)
{Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==54)
{Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==53)
{Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==52)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==51)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");Engendro2(c-3,d);_sleep(3);} 
if(x==50)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==49)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==48)
{Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==47)
{Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==46)
{Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==45)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==44)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==43)
{Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==42)
{Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==41)
{Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==40)
{Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==39)
{Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==38)
{Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==37)
{Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}	
}
for(int x=7;x<=36;x++)
{int y=11;
 Console::SetCursorPosition(x,y);
if(x==7)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");Engendro1(c-4,d);_sleep(3);}
if(x==8)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==9)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==10)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==11)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==12)
   {Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==13)
   {Console::ForegroundColor=ConsoleColor::Yellow; printf("+");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==14)
   {Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==15)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==16)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==17)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==18)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==19)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==20)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==21)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==22)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");Engendro2(c-5,d);_sleep(3);}
if(x==23)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==24)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==25)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==26)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==27)
   {Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==28)
   {Console::ForegroundColor=ConsoleColor::Yellow; printf("+");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==29)
   {Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==30)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==31)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==32)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==33)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==34)
   {Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}

if(x==35)
   {Console::ForegroundColor=ConsoleColor::Yellow; printf("+");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==36)
   {Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
}
for(int x=66;x>=37;x--)
{int y=11;
 Console::SetCursorPosition(x,y);
if(x==66)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");Engendro1(c-6,d);_sleep(3);}
if(x==65)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==64)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==63)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==62)
{Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==61)
{Console::ForegroundColor=ConsoleColor::Yellow; printf("+");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==60)
{Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==59)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==58)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==57)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==56)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==55)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==54)
{Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==53)
{Console::ForegroundColor=ConsoleColor::Yellow; printf("+");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==52)
{Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==51)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");Engendro2(c-7,d);_sleep(3);} 
if(x==50)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==49)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==48)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==47)
{Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==46)
{Console::ForegroundColor=ConsoleColor::Yellow; printf("+");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==45)
{Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==44)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==43)
{Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==42)
{Console::ForegroundColor=ConsoleColor::Yellow; printf("+");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==41)
{Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==40)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==39)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==38)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==37)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
}
for(int x=7;x<=36;x++)
{int y=12;
 Console::SetCursorPosition(x,y);
if(x==7)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");Engendro1(c-8,d);_sleep(3);}
if(x==8)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==9)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==10)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==11)
   {Console::ForegroundColor=ConsoleColor::Yellow; printf("+");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==12)
   {Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==13)
   {Console::ForegroundColor=ConsoleColor::Yellow; printf("+");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==14)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==15)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==16)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==17)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==18)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==19)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==20)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==21)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==22)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");Engendro2(c-9,d);_sleep(3);}
if(x==23)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==24)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==25)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==26)
   {Console::ForegroundColor=ConsoleColor::Yellow; printf("+");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==27)
   {Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==28)
   {Console::ForegroundColor=ConsoleColor::Yellow; printf("+");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==29)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==30)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==31)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==32)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==33)
   {Console::ForegroundColor=ConsoleColor::Yellow; printf("+");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==34)
   {Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==35)
   {Console::ForegroundColor=ConsoleColor::Yellow; printf("+");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==36)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
}
for(int x=66;x>=37;x--)
{int y=12;
 Console::SetCursorPosition(x,y);
if(x==66)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");Engendro1(c-10,d);_sleep(3);}
if(x==65)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==64)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==63)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==62)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==61)
{Console::ForegroundColor=ConsoleColor::Yellow; printf("+");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==60)
{Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==59)
{Console::ForegroundColor=ConsoleColor::Yellow; printf("+");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==58)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==57)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==56)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==55)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==54)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==53)
{Console::ForegroundColor=ConsoleColor::Yellow; printf("+");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==52)
{Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==51)
{Console::ForegroundColor=ConsoleColor::Yellow; printf("+");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");Engendro2(c-11,d);_sleep(3);} 
if(x==50)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==49)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==48)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==47)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==46)
{Console::ForegroundColor=ConsoleColor::Yellow; printf("+");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==45)
{Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==44)
{Console::ForegroundColor=ConsoleColor::Yellow; printf("+");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==43)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==42)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==41)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==40)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==39)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==38)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==37)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
}
for(int x=7;x<=36;x++)
{int y=13;
 Console::SetCursorPosition(x,y);
if(x==7)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");Engendro1(c-12,d);_sleep(3);}
if(x==8)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==9)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==10)
   {Console::ForegroundColor=ConsoleColor::Yellow; printf("+");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==11)
   {Console::ForegroundColor=ConsoleColor::Red; printf("#");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==12)
   {Console::ForegroundColor=ConsoleColor::Yellow; printf("+");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==13)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==14)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==15)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==16)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==17)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==18)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==19)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==20)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==21)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==22)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");Engendro2(c-13,d);_sleep(3);}
if(x==23)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==24)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==25)
   {Console::ForegroundColor=ConsoleColor::Yellow; printf("+");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==26)
   {Console::ForegroundColor=ConsoleColor::Red; printf("#");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==27)
   {Console::ForegroundColor=ConsoleColor::Yellow; printf("+");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==28)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==29)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==30)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==31)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==32)
   {Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==33)
   {Console::ForegroundColor=ConsoleColor::Red; printf("#");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==34)
   {Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}

if(x==35)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==36)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");Engendro1(c-14,d);_sleep(3);}
}
for(int x=66;x>=37;x--)
{int y=13;
 Console::SetCursorPosition(x,y);
if(x==66)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");Engendro1(c-14,d);_sleep(3);}
if(x==65)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==64)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==63)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==62)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==61)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==60)
{Console::ForegroundColor=ConsoleColor::Yellow; printf("+");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==59)
{Console::ForegroundColor=ConsoleColor::Red; printf("#");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==58)
{Console::ForegroundColor=ConsoleColor::Yellow; printf("+");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==57)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==56)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==55)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==54)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==53)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==52)
{Console::ForegroundColor=ConsoleColor::Yellow; printf("+");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==51)
{Console::ForegroundColor=ConsoleColor::Yellow; printf("+");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");MataEngendro(c-14,d);Engendro2(c-13,d);_sleep(3);} 
if(x==50)
{Console::ForegroundColor=ConsoleColor::Red; printf("#");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==49)
{Console::ForegroundColor=ConsoleColor::Yellow; printf("+");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==48)
{Console::ForegroundColor=ConsoleColor::Yellow; printf("+");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==47)
{Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==46)
{Console::ForegroundColor=ConsoleColor::Yellow; printf("+");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==45)
{Console::ForegroundColor=ConsoleColor::Yellow; printf("+");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==44)
{Console::ForegroundColor=ConsoleColor::Red; printf("#");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==43)
{Console::ForegroundColor=ConsoleColor::Yellow; printf("+");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==42)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==41)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==40)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==39)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==38)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==37)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");MataEngendro(c-13,d);Engendro1(c-12,d);_sleep(3);}	
}
for(int x=7;x<=36;x++)
{int y=14;
 Console::SetCursorPosition(x,y);
if(x==7)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");MataEngendro(c-12,d);Engendro2(c-11,d);_sleep(3);}
if(x==8)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==9)
   {Console::ForegroundColor=ConsoleColor::Yellow; printf("+");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==10)
   {Console::ForegroundColor=ConsoleColor::Red; printf("#");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==11)
   {Console::ForegroundColor=ConsoleColor::Yellow; printf("+");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==12)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==13)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==14)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==15)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==16)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==17)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==18)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==19)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==20)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==21)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==22)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");MataEngendro(c-11,d);Engendro1(c-10,d);_sleep(3);}
if(x==23)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==24)
   {Console::ForegroundColor=ConsoleColor::Yellow; printf("+");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==25)
   {Console::ForegroundColor=ConsoleColor::Red; printf("#");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==26)
   {Console::ForegroundColor=ConsoleColor::Yellow; printf("+");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==27)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==28)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==29)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==30)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==31)
   {Console::ForegroundColor=ConsoleColor::Yellow; printf("+");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==32)
   {Console::ForegroundColor=ConsoleColor::Red; printf("#");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==33)
   {Console::ForegroundColor=ConsoleColor::Yellow; printf("+");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==34)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==35)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==36)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
}
for(int x=66;x>=37;x--)
{int y=14;
 Console::SetCursorPosition(x,y);
if(x==66)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");MataEngendro(c-10,d);Engendro2(c-9,d);_sleep(3);}
if(x==65)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==64)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==63)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==62)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==61)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==60)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==59)
{Console::ForegroundColor=ConsoleColor::Yellow; printf("+");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==58)
{Console::ForegroundColor=ConsoleColor::Red; printf("#");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==57)
{Console::ForegroundColor=ConsoleColor::Yellow; printf("+");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==56)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==55)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==54)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==53)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==52)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==51)
{Console::ForegroundColor=ConsoleColor::Yellow; printf("+");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");MataEngendro(c-9,d);Engendro1(c-8,d);_sleep(3);} 
if(x==50)
{Console::ForegroundColor=ConsoleColor::Red; printf("#");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==49)
{Console::ForegroundColor=ConsoleColor::Yellow; printf("+");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==48)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==47)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==46)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==45)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==44)
{Console::ForegroundColor=ConsoleColor::Yellow; printf("+");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==43)
{Console::ForegroundColor=ConsoleColor::Red; printf("#");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==42)
{Console::ForegroundColor=ConsoleColor::Yellow; printf("+");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==41)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==40)
{Console::ForegroundColor=ConsoleColor::Red; printf("#");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==39)
{Console::ForegroundColor=ConsoleColor::Yellow; printf("+");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==38)
{Console::ForegroundColor=ConsoleColor::Red; printf("#");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==37)
{Console::ForegroundColor=ConsoleColor::Yellow; printf("+");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}	
}
for(int x=7;x<=36;x++)
{int y=15;
 Console::SetCursorPosition(x,y);
if(x==7)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");MataEngendro(c-8,d);Engendro2(c-7,d);_sleep(3);}
if(x==8)
   {Console::ForegroundColor=ConsoleColor::Red; printf("#");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==9)
   {Console::ForegroundColor=ConsoleColor::Yellow; printf("+");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==10)
   {Console::ForegroundColor=ConsoleColor::Red; printf("#");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==11)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==12)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==13)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==14)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==15)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==16)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==17)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==18)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==19)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==20)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==21)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==22)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");MataEngendro(c-7,d);Engendro1(c-6,d);_sleep(3);}
if(x==23)
   {Console::ForegroundColor=ConsoleColor::Red; printf("#");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==24)
   {Console::ForegroundColor=ConsoleColor::Yellow; printf("+");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==25)
   {Console::ForegroundColor=ConsoleColor::Red; printf("#");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==26)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==27)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==28)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==29)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==30)
   {Console::ForegroundColor=ConsoleColor::Red; printf("#");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==31)
   {Console::ForegroundColor=ConsoleColor::Yellow; printf("+");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==32)
   {Console::ForegroundColor=ConsoleColor::Red; printf("#");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==33)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==34)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==35)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==36)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
}
for(int x=66;x>=37;x--)
{int y=15;
 Console::SetCursorPosition(x,y);
if(x==66)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");MataEngendro(c-6,d);Engendro2(c-5,d);_sleep(3);}
if(x==65)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==64)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==63)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==62)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==61)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==60)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==59)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==58)
{Console::ForegroundColor=ConsoleColor::Red; printf("#");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==57)
{Console::ForegroundColor=ConsoleColor::Yellow; printf("+");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==56)
{Console::ForegroundColor=ConsoleColor::Red; printf("#");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==55)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==54)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==53)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==52)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==51)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");MataEngendro(c-5,d);Engendro1(c-4,d);_sleep(3);} 
if(x==50)
{Console::ForegroundColor=ConsoleColor::Red; printf("#");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==49)
{Console::ForegroundColor=ConsoleColor::Yellow; printf("+");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==48)
{Console::ForegroundColor=ConsoleColor::Red; printf("#");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==47)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==46)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==45)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==44)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==43)
{Console::ForegroundColor=ConsoleColor::Red; printf("#");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==42)
{Console::ForegroundColor=ConsoleColor::Yellow; printf("+");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==41)
{Console::ForegroundColor=ConsoleColor::Red; printf("#");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==40)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==39)
{Console::ForegroundColor=ConsoleColor::Red; printf("#");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==38)
{Console::ForegroundColor=ConsoleColor::Yellow; printf("+");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==37)
{Console::ForegroundColor=ConsoleColor::Red; printf("#");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
}
for(int x=7;x<=36;x++)
{int y=16;
 Console::SetCursorPosition(x,y);
if(x==7)
   {Console::ForegroundColor=ConsoleColor::Red; printf("#");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");MataEngendro(c-4,d);Engendro2(c-3,d);_sleep(3);}
if(x==8)
   {Console::ForegroundColor=ConsoleColor::Red; printf("#");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==9)
   {Console::ForegroundColor=ConsoleColor::Red; printf("#");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==10)
   {Console::ForegroundColor=ConsoleColor::Red; printf("#");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==11)
   {Console::ForegroundColor=ConsoleColor::Red; printf("#");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==12)
   {Console::ForegroundColor=ConsoleColor::Red; printf("#");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==13)
   {Console::ForegroundColor=ConsoleColor::Red; printf("#");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==14)
   {Console::ForegroundColor=ConsoleColor::Red; printf("#");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==15)
   {Console::ForegroundColor=ConsoleColor::Red; printf("#");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==16)
   {Console::ForegroundColor=ConsoleColor::Red; printf("#");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==17)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==18)
   {Console::ForegroundColor=ConsoleColor::Red; printf("#");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==19)
   {Console::ForegroundColor=ConsoleColor::Red; printf("#");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==20)
   {Console::ForegroundColor=ConsoleColor::Red; printf("#");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==21)
   {Console::ForegroundColor=ConsoleColor::Red; printf("#");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==22)
   {Console::ForegroundColor=ConsoleColor::Red; printf("#");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");MataEngendro(c-3,d);Engendro1(c-2,d);_sleep(3);}
if(x==23)
   {Console::ForegroundColor=ConsoleColor::Red; printf("#");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==24)
   {Console::ForegroundColor=ConsoleColor::Red; printf("#");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==25)
   {Console::ForegroundColor=ConsoleColor::Red; printf("#");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==26)
   {Console::ForegroundColor=ConsoleColor::Red; printf("#");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==27)
   {Console::ForegroundColor=ConsoleColor::Red; printf("#");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==28)
   {Console::ForegroundColor=ConsoleColor::Red; printf("#");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==29)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==30)
   {Console::ForegroundColor=ConsoleColor::Red; printf("#");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==31)
   {Console::ForegroundColor=ConsoleColor::Red; printf("#");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==32)
   {Console::ForegroundColor=ConsoleColor::Red; printf("#");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==33)
   {Console::ForegroundColor=ConsoleColor::Red; printf("#");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==34)
   {Console::ForegroundColor=ConsoleColor::Red; printf("#");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==35)
   {Console::ForegroundColor=ConsoleColor::Red; printf("#");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==36)
   {Console::ForegroundColor=ConsoleColor::Red; printf("#");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
}
for(int x=66;x>=37;x--)
{int y=16;
 Console::SetCursorPosition(x,y);
if(x==66)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");MataEngendro(c-2,d);Engendro2(c-1,d);_sleep(3);}
if(x==65)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==64)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==63)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==62)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==61)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==60)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==59)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==58)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==57)
{Console::ForegroundColor=ConsoleColor::Red; printf("#");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==56)
{Console::ForegroundColor=ConsoleColor::Red; printf("#");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==55)
{Console::ForegroundColor=ConsoleColor::Red; printf("#");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==54)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==53)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==52)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==51)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");MataEngendro(c-1,d);Engendro1(c,d);_sleep(3);} 
if(x==50)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==49)
{Console::ForegroundColor=ConsoleColor::Red; printf("#");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==48)
{Console::ForegroundColor=ConsoleColor::Red; printf("#");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==47)
{Console::ForegroundColor=ConsoleColor::Red; printf("#");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==46)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==45)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==44)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==43)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==42)
{Console::ForegroundColor=ConsoleColor::Red; printf("#");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==41)
{Console::ForegroundColor=ConsoleColor::Red; printf("#");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==40)
{Console::ForegroundColor=ConsoleColor::Red; printf("#");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==39)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==38)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==37)
{Console::ForegroundColor=ConsoleColor::Red; printf("#");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}	
}
for(int x=7;x<=36;x++)
{int y=17;
 Console::SetCursorPosition(x,y);
if(x==7)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");Engendro1(c,d);_sleep(3);}
if(x==8)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==9)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==10)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==11)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==12)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==13)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==14)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==15)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==16)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==17)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==18)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==19)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==20)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==21)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==22)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");Engendro2(c-1,d);_sleep(3);}
if(x==23)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==24)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==25)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==26)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==27)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==28)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==29)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==30)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==31)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==32)
   {Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==33)
   {Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==34)
   {Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}

if(x==35)
   {Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==36)
   {Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
}
for(int x=66;x>=37;x--)
{int y=17;
 Console::SetCursorPosition(x,y);
if(x==66)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");Engendro1(c-2,d);_sleep(3);}
if(x==65)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==64)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==63)
{Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==62)
{Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==61)
{Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==60)
{Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==59)
{Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==58)
{Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==57)
{Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==56)
{Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==55)
{Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==54)
{Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==53)
{Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==52)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==51)
{Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");Engendro2(c-3,d);_sleep(3);} 
if(x==50)
{Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==49)
{Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==48)
{Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==47)
{Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==46)
{Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==45)
{Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==44)
{Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==43)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==42)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==41)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==40)
{Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==39)
{Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==38)
{Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==37)
{Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}	
}
for(int x=7;x<=36;x++)
{int y=18;
 Console::SetCursorPosition(x,y);
if(x==7)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");Engendro1(c-4,d);_sleep(3);}
if(x==8)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==9)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==10)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==11)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==12)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==13)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==14)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==15)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==16)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==17)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==18)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==19)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==20)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==21)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==22)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");Engendro2(c-5,d);_sleep(3);}
if(x==23)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==24)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==25)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==26)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==27)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==28)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==29)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==30)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==31)
   {Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==32)
   {Console::ForegroundColor=ConsoleColor::Yellow; printf("+");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==33)
   {Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==34)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==35)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==36)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
}
for(int x=66;x>=37;x--)
{int y=18;
 Console::SetCursorPosition(x,y);
if(x==66)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");Engendro1(c-6,d);_sleep(3);}
if(x==65)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==64)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==63)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==62)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==61)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==60)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==59)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==58)
{Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==57)
{Console::ForegroundColor=ConsoleColor::Yellow; printf("+");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==56)
{Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==55)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==54)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==53)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==52)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==51)
{Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");Engendro2(c-7,d);_sleep(3);} 
if(x==50)
{Console::ForegroundColor=ConsoleColor::Yellow; printf("+");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==49)
{Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==48)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==47)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==46)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==45)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==44)
{Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==43)
{Console::ForegroundColor=ConsoleColor::Yellow; printf("+");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==42)
{Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==41)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==40)
{Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==39)
{Console::ForegroundColor=ConsoleColor::Yellow; printf("+");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==38)
{Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==37)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}	
}
for(int x=7;x<=36;x++)
{int y=19;
 Console::SetCursorPosition(x,y);
if(x==7)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");Engendro1(c-8,d);_sleep(3);}
if(x==8)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==9)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==10)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==11)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==12)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==13)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==14)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==15)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==16)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==17)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==18)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==19)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==20)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==21)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==22)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");Engendro2(c-9,d);_sleep(3);}
if(x==23)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==24)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==25)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==26)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==27)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==28)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==29)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==30)
   {Console::ForegroundColor=ConsoleColor::Yellow; printf("+");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==31)
   {Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==32)
   {Console::ForegroundColor=ConsoleColor::Yellow; printf("+");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==33)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==34)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==35)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==36)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
}
for(int x=66;x>=37;x--)
{int y=19;
 Console::SetCursorPosition(x,y);
if(x==66)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");Engendro1(c-10,d);_sleep(3);}
if(x==65)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==64)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==63)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==62)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==61)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==60)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==59)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==58)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==57)
{Console::ForegroundColor=ConsoleColor::Yellow; printf("+");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==56)
{Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==55)
{Console::ForegroundColor=ConsoleColor::Yellow; printf("+");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==54)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==53)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==52)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==51)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");Engendro2(c-11,d);_sleep(3);} 
if(x==50)
{Console::ForegroundColor=ConsoleColor::Yellow; printf("+");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==49)
{Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==48)
{Console::ForegroundColor=ConsoleColor::Yellow; printf("+");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==47)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==46)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==45)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==44)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==43)
{Console::ForegroundColor=ConsoleColor::Yellow; printf("+");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==42)
{Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==41)
{Console::ForegroundColor=ConsoleColor::Yellow; printf("+");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==40)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==39)
{Console::ForegroundColor=ConsoleColor::Yellow; printf("+");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==38)
{Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==37)
{Console::ForegroundColor=ConsoleColor::Yellow; printf("+");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}	
}
for(int x=7;x<=36;x++)
{int y=20;
 Console::SetCursorPosition(x,y);
if(x==7)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");Engendro1(c-12,d);_sleep(3);}
if(x==8)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==9)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==10)
   {Console::ForegroundColor=ConsoleColor::Yellow; printf("+");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==11)
   {Console::ForegroundColor=ConsoleColor::Red; printf("#");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==12)
   {Console::ForegroundColor=ConsoleColor::Yellow; printf("+");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==13)
   {Console::ForegroundColor=ConsoleColor::Yellow; printf("+");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==14)
   {Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==15)
   {Console::ForegroundColor=ConsoleColor::Yellow; printf("+");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==16)
   {Console::ForegroundColor=ConsoleColor::Yellow; printf("+");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==17)
   {Console::ForegroundColor=ConsoleColor::Red; printf("#");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==18)
   {Console::ForegroundColor=ConsoleColor::Yellow; printf("+");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==19)
   {Console::ForegroundColor=ConsoleColor::Yellow; printf("+");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==20)
   {Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==21)
   {Console::ForegroundColor=ConsoleColor::Yellow; printf("+");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==22)
   {Console::ForegroundColor=ConsoleColor::Yellow; printf("+");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");Engendro2(c-13,d);_sleep(3);}
if(x==23)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==24)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==25)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==26)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==27)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==28)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==29)
   {Console::ForegroundColor=ConsoleColor::Yellow; printf("+");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==30)
   {Console::ForegroundColor=ConsoleColor::Red; printf("#");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==31)
   {Console::ForegroundColor=ConsoleColor::Yellow; printf("+");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==32)
   {Console::ForegroundColor=ConsoleColor::Yellow; printf("+");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==33)
   {Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==34)
   {Console::ForegroundColor=ConsoleColor::Yellow; printf("+");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==35)
   {Console::ForegroundColor=ConsoleColor::Yellow; printf("+");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==36)
   {Console::ForegroundColor=ConsoleColor::Red; printf("#");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");Engendro1(c-14,d);_sleep(3);}
}
for(int x=66;x>=37;x--)
{int y=20;
 Console::SetCursorPosition(x,y);
if(x==66)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");Engendro1(c-14,d);_sleep(3);}
if(x==65)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==64)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==63)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==62)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==61)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==60)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==59)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==58)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==57)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==56)
{Console::ForegroundColor=ConsoleColor::Yellow; printf("+");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==55)
{Console::ForegroundColor=ConsoleColor::Red; printf("#");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==54)
{Console::ForegroundColor=ConsoleColor::Yellow; printf("+");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==53)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==52)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==51)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");MataEngendro(c-14,d);Engendro2(c-13,d);_sleep(3);} 
if(x==50)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==49)
{Console::ForegroundColor=ConsoleColor::Yellow; printf("+");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==48)
{Console::ForegroundColor=ConsoleColor::White; printf(":");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==47)
{Console::ForegroundColor=ConsoleColor::Yellow; printf("+");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==46)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==45)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==44)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==43)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==42)
{Console::ForegroundColor=ConsoleColor::Yellow; printf("+");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==41)
{Console::ForegroundColor=ConsoleColor::Red; printf("#");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==40)
{Console::ForegroundColor=ConsoleColor::Yellow; printf("+");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==39)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==38)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==37)
{Console::ForegroundColor=ConsoleColor::Yellow; printf("+");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");MataEngendro(c-13,d);Engendro1(c-12,d);_sleep(3);}
}
for(int x=7;x<=36;x++)
{int y=21;
 Console::SetCursorPosition(x,y);
if(x==7)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");MataEngendro(c-12,d);Engendro2(c-11,d);_sleep(3);}
if(x==8)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==9)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==10)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==11)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==12)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==13)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==14)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==15)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==16)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==17)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==18)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==19)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==20)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==21)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==22)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");MataEngendro(c-11,d);Engendro1(c-10,d);_sleep(3);}
if(x==23)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==24)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==25)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==26)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==27)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==28)
   {Console::ForegroundColor=ConsoleColor::Yellow; printf("+");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==29)
   {Console::ForegroundColor=ConsoleColor::Red; printf("#");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==30)
   {Console::ForegroundColor=ConsoleColor::Yellow; printf("+");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==31)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==32)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==33)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==34)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==35)
   {Console::ForegroundColor=ConsoleColor::Yellow; printf("+");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==36)
   {Console::ForegroundColor=ConsoleColor::Red; printf("#");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
}
for(int x=66;x>=37;x--)
{int y=21;
 Console::SetCursorPosition(x,y);
if(x==66)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");MataEngendro(c-10,d);Engendro2(c-9,d);_sleep(3);}
if(x==65)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==64)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==63)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==62)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==61)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==60)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==59)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==58)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==57)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==56)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==55)
{Console::ForegroundColor=ConsoleColor::Yellow; printf("+");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==54)
{Console::ForegroundColor=ConsoleColor::Red; printf("#");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==53)
{Console::ForegroundColor=ConsoleColor::Yellow; printf("+");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==52)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==51)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");MataEngendro(c-9,d);Engendro1(c-8,d);_sleep(3);} 
if(x==50)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==49)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==48)
{Console::ForegroundColor=ConsoleColor::Yellow; printf("+");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==47)
{Console::ForegroundColor=ConsoleColor::Red; printf("#");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 	
if(x==46)
{Console::ForegroundColor=ConsoleColor::Yellow; printf("+");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==45)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==44)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==43)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==42)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==41)
{Console::ForegroundColor=ConsoleColor::Yellow; printf("+");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==40)
{Console::ForegroundColor=ConsoleColor::Red; printf("#");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==39)
{Console::ForegroundColor=ConsoleColor::Yellow; printf("+");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==38)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==37)
{Console::ForegroundColor=ConsoleColor::Yellow; printf("+");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
}
for(int x=7;x<=36;x++)
{int y=22;
 Console::SetCursorPosition(x,y);
if(x==7)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");MataEngendro(c-8,d);Engendro2(c-7,d);_sleep(3);}
if(x==8)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==9)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==10)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==11)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==12)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==13)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==14)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==15)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==16)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==17)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==18)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==19)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==20)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==21)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==22)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");MataEngendro(c-7,d);Engendro1(c-6,d);_sleep(3);}
if(x==23)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==24)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==25)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==26)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==27)
   {Console::ForegroundColor=ConsoleColor::Red; printf("#");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==28)
   {Console::ForegroundColor=ConsoleColor::Yellow; printf("+");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==29)
   {Console::ForegroundColor=ConsoleColor::Red; printf("#");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==30)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==31)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==32)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==33)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==34)
   {Console::ForegroundColor=ConsoleColor::Red; printf("#");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==35)
   {Console::ForegroundColor=ConsoleColor::Yellow; printf("+");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==36)
   {Console::ForegroundColor=ConsoleColor::Red; printf("#");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
}
for(int x=66;x>=37;x--)
{int y=22;
 Console::SetCursorPosition(x,y);
if(x==66)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");MataEngendro(c-6,d);Engendro2(c-5,d);_sleep(3);}
if(x==65)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==64)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==63)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==62)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==61)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==60)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==59)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==58)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==57)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==56)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==55)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==54)
{Console::ForegroundColor=ConsoleColor::Red; printf("#");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==53)
{Console::ForegroundColor=ConsoleColor::Yellow; printf("+");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==52)
{Console::ForegroundColor=ConsoleColor::Red; printf("#");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==51)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");MataEngendro(c-5,d);Engendro1(c-4,d);_sleep(3);} 
if(x==50)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==49)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==48)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==47)
{Console::ForegroundColor=ConsoleColor::Red; printf("#");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 	
if(x==46)
{Console::ForegroundColor=ConsoleColor::Yellow; printf("+");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==45)
{Console::ForegroundColor=ConsoleColor::Red; printf("#");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==44)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==43)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==42)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==41)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==40)
{Console::ForegroundColor=ConsoleColor::Red; printf("#");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==39)
{Console::ForegroundColor=ConsoleColor::Yellow; printf("+");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==38)
{Console::ForegroundColor=ConsoleColor::Red; printf("#");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==37)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
}
for(int x=7;x<=36;x++)
{int y=23;
 Console::SetCursorPosition(x,y);
if(x==7)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");MataEngendro(c-4,d);Engendro2(c-3,d);_sleep(3);}
if(x==8)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==9)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==10)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==11)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==12)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==13)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==14)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==15)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==16)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==17)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==18)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==19)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==20)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==21)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==22)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");MataEngendro(c-3,d);Engendro1(c-2,d);_sleep(3);}
if(x==23)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==24)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==25)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==26)
   {Console::ForegroundColor=ConsoleColor::Red; printf("#");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==27)
   {Console::ForegroundColor=ConsoleColor::Red; printf("#");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==28)
   {Console::ForegroundColor=ConsoleColor::Red; printf("#");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==29)
   {Console::ForegroundColor=ConsoleColor::Red; printf("#");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==30)
   {Console::ForegroundColor=ConsoleColor::Red; printf("#");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==31)
   {Console::ForegroundColor=ConsoleColor::Red; printf("#");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==32)
   {Console::ForegroundColor=ConsoleColor::Red; printf("#");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==33)
   {Console::ForegroundColor=ConsoleColor::Red; printf("#");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==34)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==35)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==36)
   {printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
}
for(int x=66;x>=37;x--)
{int y=23;
 Console::SetCursorPosition(x,y);
if(x==66)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");MataEngendro(c-2,d);Engendro2(c-1,d);_sleep(3);}
if(x==65)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
if(x==64)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==63)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==62)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==61)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==60)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==59)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==58)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==57)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==56)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==55)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==54)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==53)
{Console::ForegroundColor=ConsoleColor::Red; printf("#");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==52)
{Console::ForegroundColor=ConsoleColor::Red; printf("#");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==51)
{Console::ForegroundColor=ConsoleColor::Red; printf("#");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");MataEngendro(c-1,d);Engendro1(c,d);_sleep(3);} 
if(x==50)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==49)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==48)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==47)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 	
if(x==46)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==45)
{Console::ForegroundColor=ConsoleColor::Red; printf("#");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==44)
{Console::ForegroundColor=ConsoleColor::Red; printf("#");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==43)
{Console::ForegroundColor=ConsoleColor::Red; printf("#");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==42)
{Console::ForegroundColor=ConsoleColor::Red; printf("#");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==41)
{Console::ForegroundColor=ConsoleColor::Red; printf("#");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==40)
{Console::ForegroundColor=ConsoleColor::Red; printf("#");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==39)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==38)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);} 
if(x==37)
{printf(" ");Console::SetCursorPosition(a,b);Console::ForegroundColor=ConsoleColor::White; printf("Presione Enter...");_sleep(3);}
}
}
void Intro()
{
int te=0;
do{
	Pantalla2();
	if(_kbhit()){
		te=_getch();}
	if(te==ENTER){
			Console::Clear();
			break;}
	else{
		Console::Clear();
		Pantalla1();
		if(_kbhit())
		te=_getch();
		Console::Clear();}
}while(te!=ENTER);
}
void Final()
{
int a,b;
a=60;b=39;
for(int y=0;y<=39;y++)
{int x=7;
 Console::SetCursorPosition(x,y);  
 if(y==16)
 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
}
for(int y=0;y<=39;y++)
{int x=66;
 Console::SetCursorPosition(x,y);
  if(y==10)
 {Console::ForegroundColor=ConsoleColor::Yellow;	
	 printf(":");
 }

 }
_sleep(50);
for(int y=0;y<=39;y++)
{int x=8;
 Console::SetCursorPosition(x,y);
 if(y==15)
{Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==16)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");} 
 }
for(int y=0;y<=39;y++)
{int x=65;
 Console::SetCursorPosition(x,y);  
  if(y==10)
 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
}
  _sleep(50);
for(int y=0;y<=39;y++)
{int x=9;
 Console::SetCursorPosition(x,y);  
 if(y==14)
 {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==15)
	{Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==16)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");} 
}
for(int y=0;y<=39;y++)
{int x=64;
 Console::SetCursorPosition(x,y);  
if(y==10)
 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}

}
_sleep(50);
for(int y=0;y<=39;y++)
{int x=10;
 Console::SetCursorPosition(x,y);  
 if(y==13)
	 {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if (y==14)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==15)
	{Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==16)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==20)
	 {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
}
for(int y=0;y<=39;y++)
{int x=63;
 Console::SetCursorPosition(x,y);
 if(y==10)
 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==17)
 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}

}
_sleep(50);
for(int y=0;y<=39;y++)
{int x=11;
 Console::SetCursorPosition(x,y);  
 if(y==12)
	 {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==13)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==14)
	 {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==16)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==20)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
}
for(int y=0;y<=39;y++)
{int x=62;
 Console::SetCursorPosition(x,y);  
 if(y==10)
 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==11)
 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==17)
 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}

}
_sleep(50);
for(int y=0;y<=39;y++)
{int x=12;
 Console::SetCursorPosition(x,y);  
 if(y==11)
 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==12)
 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==13)
	{Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==16)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==20)
	 {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
}
for(int y=0;y<=39;y++)
{int x=61;
 Console::SetCursorPosition(x,y);  
 if(y==10)
 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==11)
	 {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==12)
	 {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==17)
 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}

}
_sleep(50);
for(int y=0;y<=39;y++)
{int x=13;
 Console::SetCursorPosition(x,y);  
 if(y==10)
 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==11)
	{Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==12)
	 {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==16)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==20)
	 {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
}
for(int y=0;y<=39;y++)
{int x=60;
 Console::SetCursorPosition(x,y);
 if(y==10)
 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==11)
 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==12)
 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==13)
	 {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==17)
 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
}
_sleep(50);
for(int y=0;y<=39;y++)
{int x=14;
 Console::SetCursorPosition(x,y);  
 if(y==10)
 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==11)
 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==16)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==20)
 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
}
for(int y=0;y<=39;y++)
{int x=59;
 Console::SetCursorPosition(x,y);
 if(y==10)
 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==12)
	 {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==13)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==14)
	 {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==17)
 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}

}
_sleep(50);
for(int y=0;y<=39;y++)
{int x=15;
 Console::SetCursorPosition(x,y);  
 if(y==10)
 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==16)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==20)
	 {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
}
for(int y=0;y<=39;y++)
{int x=58;
 Console::SetCursorPosition(x,y);
 if(y==10)
 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==13)
	 {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==14)
	{Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==15)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==17)
 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==18)
{Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}

}
_sleep(50);
for(int y=0;y<=39;y++)
{int x=16;
 Console::SetCursorPosition(x,y);  
 if(y==16)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==20)
	 {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
}
for(int y=0;y<=39;y++)
{int x=57;
 Console::SetCursorPosition(x,y);
 if(y==10)
 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==14)
{Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==15)
	 {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==16)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==17)
 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==18)
	 {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==19)
 {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
}
_sleep(50);
for(int y=0;y<=39;y++)
{int x=17;
 Console::SetCursorPosition(x,y);  
 if(y==20)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
}
for(int y=0;y<=39;y++)
{int x=56;
 Console::SetCursorPosition(x,y);
 if(y==15)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==16)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==17)
 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==18)
{Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==19)
	{Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==20)
	 {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}

}
_sleep(50);
for(int y=0;y<=39;y++)
{int x=18;
 Console::SetCursorPosition(x,y);  
 if(y==16)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==20)
	 {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
}
for(int y=0;y<=39;y++)
{int x=55;
 Console::SetCursorPosition(x,y);
 if(y==10)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==16)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==17)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==19)
	 {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==20)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==21)
	 {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}

}
_sleep(50);
for(int y=0;y<=39;y++)
{int x=19;
 Console::SetCursorPosition(x,y);  
 if(y==16)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==20)
	 {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
}
for(int y=0;y<=39;y++)
{int x=54;
 Console::SetCursorPosition(x,y);
 if(y==10)
{Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==11)
	{Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==17)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==20)
	 {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==21)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==22)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
}
_sleep(50);
for(int y=0;y<=39;y++)
{int x=20;
 Console::SetCursorPosition(x,y);  
 if(y==16)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==20)
	{Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");};
}
for(int y=0;y<=39;y++)
{int x=53;
 Console::SetCursorPosition(x,y);
 if(y==10)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==11)
	 {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==12)
	 {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==17)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==21)
	 {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==22)
	 {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==23)
	{Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}

}
_sleep(50);
for(int y=0;y<=39;y++)
{int x=21;
 Console::SetCursorPosition(x,y);  
 if(y==16)
	{Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==20)
	 {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
}
for(int y=0;y<=39;y++)
{int x=52;
 Console::SetCursorPosition(x,y);
 if(y==11)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==12)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==13)
	 {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==22)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==23)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}

}
_sleep(50);
for(int y=0;y<=39;y++)
{int x=22;
 Console::SetCursorPosition(x,y);  
 if(y==16)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==20)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
}
for(int y=0;y<=39;y++)
{int x=51;
 Console::SetCursorPosition(x,y);
 if(y==12)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==13)
	 {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==14)
	 {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==17)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==18)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");} 
 if(y==23)
	{Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}

}
_sleep(50);
for(int y=0;y<=39;y++)
{int x=23;
 Console::SetCursorPosition(x,y);  
 if(y==15)
	{Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==16)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
}
for(int y=0;y<=39;y++)
{int x=50;
 Console::SetCursorPosition(x,y);
 if(y==13)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==14)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==15)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==17)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==18)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==19)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}

}
_sleep(50);
for(int y=0;y<=39;y++)
{int x=24;
 Console::SetCursorPosition(x,y);  
 if(y==10)
	{Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==14)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==15)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==16)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
}
for(int y=0;y<=39;y++)
{int x=49;
 Console::SetCursorPosition(x,y);
 if(y==13)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==14)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==15)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==16)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==17)
	{Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==18)
	{Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==19)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==20)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 }
_sleep(50);
for(int y=0;y<=39;y++)
{int x=25;
 Console::SetCursorPosition(x,y);  
 if(y==10)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==13)
	 {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==14)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==15)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==16)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
}
for(int y=0;y<=39;y++)
{int x=48;
 Console::SetCursorPosition(x,y);
 if(y==10)
{Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==13)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==15)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==16)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==17)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==19)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==20)
	{Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==21)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
}
_sleep(50);
for(int y=0;y<=39;y++)
{int x=26;
 Console::SetCursorPosition(x,y);  
 if(y==10)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==12)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==13)
	{Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==14)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==16)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==23)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
}
for(int y=0;y<=39;y++)
{int x=47;
 Console::SetCursorPosition(x,y);
 if(y==10)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==11)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==13)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==16)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==17)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==20)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==21)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==22)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
}
_sleep(50);
for(int y=0;y<=39;y++)
{int x=27;
 Console::SetCursorPosition(x,y);  
 if(y==10)
    {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==11)
	{Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==12)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==13)
	 {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==16)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==22)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==23)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
}
for(int y=0;y<=39;y++)
{int x=46;
 Console::SetCursorPosition(x,y);
if(y==10)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==11)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==12)
	 {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==13)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==17)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==21)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==22)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 }
_sleep(50);
for(int y=0;y<=39;y++)
{int x=28;
 Console::SetCursorPosition(x,y);  
 if(y==10)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==11)
	 {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==12)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==16)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==21)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==22)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==23)
	{Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
}
for(int y=0;y<=39;y++)
{int x=45;
 Console::SetCursorPosition(x,y);
 if(y==11)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==12)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==13)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==17)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==22)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==23)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}

}
_sleep(50);
for(int y=0;y<=39;y++)
{int x=29;
 Console::SetCursorPosition(x,y);  
 if(y==10)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==11)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==20)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==21)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==22)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==23)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
}
for(int y=0;y<=39;y++)
{int x=44;
 Console::SetCursorPosition(x,y);
 if(y==12)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==13)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==14)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==17)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==18)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==23)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}

 }
_sleep(50);
for(int y=0;y<=39;y++)
{int x=30;
 Console::SetCursorPosition(x,y);  
 if(y==10)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==15)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==16)
	{Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==19)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==20)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==21)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==23)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
}
for(int y=0;y<=39;y++)
{int x=43;
 Console::SetCursorPosition(x,y);
if(y==10)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==11)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==13)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==14)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==15)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==18)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==19)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==23)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}

}
_sleep(50);
for(int y=0;y<=39;y++)
{int x=31;
 Console::SetCursorPosition(x,y);  
 if(y==10)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==14)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==15)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==16)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==18)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==19)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==20)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==23)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
}
for(int y=0;y<=39;y++)
{int x=42;
 Console::SetCursorPosition(x,y);
 if(y==10)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==11)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==14)
 {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");} 
 if(y==15)
 {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==16)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==18)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==19)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==20)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==23)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}

 }
_sleep(50);
for(int y=0;y<=39;y++)
{int x=32;
 Console::SetCursorPosition(x,y);  
 if(y==10)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==13)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==14)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==15)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==16)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==17)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==18)
	 {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==19)
	 {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==20)
	 {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==23)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
}
for(int y=0;y<=39;y++)
{int x=41;
 Console::SetCursorPosition(x,y);
if(y==10)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==11)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==15)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==16)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==19)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==20)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==21)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==23)
	{Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
}
_sleep(50);
for(int y=0;y<=39;y++)
{int x=33;
 Console::SetCursorPosition(x,y);  
 if(y==10)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
if(y==12)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==13)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==14)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==16)
	{Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==17)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==18)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==20)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==23)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
}
for(int y=0;y<=39;y++)
{int x=40;
 Console::SetCursorPosition(x,y);
if(y==10)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==14)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==16)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==17)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==18)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==20)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==21)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==22)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==23)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
}
_sleep(50);
for(int y=0;y<=39;y++)
{int x=34;
 Console::SetCursorPosition(x,y);  
  if(y==10)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==11)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==12)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==13)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==16)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==17)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==20)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==22)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==23)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
}
for(int y=0;y<=39;y++)
{int x=39;
 Console::SetCursorPosition(x,y);
if(y==10)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==14)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==15)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==17)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==18)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==19)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==21)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==22)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==23)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}

}
_sleep(50);
for(int y=0;y<=39;y++)
{int x=35;
 Console::SetCursorPosition(x,y);  
 if(y==11)
	 {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==12)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==16)
	{Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==17)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==20)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==21)
	{Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==22)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
}
for(int y=0;y<=39;y++)
{int x=38;
 Console::SetCursorPosition(x,y);
if(y==10)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==14)
	{Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==15)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==17)
	{Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==18)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==19)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==22)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==23)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}

}
_sleep(50);
for(int y=0;y<=39;y++)
{int x=36;
 Console::SetCursorPosition(x,y);  
if(y==10)
	{Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==11)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==16)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==17)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==20)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==21)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==22)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
}
for(int y=0;y<=39;y++)
{int x=37;
 Console::SetCursorPosition(x,y);
 if(y==10)
	 {Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==14)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==15)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==16)
	 {Console::ForegroundColor=ConsoleColor::DarkRed;
 printf("#");}
 if(y==17)
	{Console::ForegroundColor=ConsoleColor::Yellow;
 printf(":");}
 if(y==19)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==20)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}
 if(y==21)
	  {Console::ForegroundColor=ConsoleColor::Red;
 printf("+");}

}
_sleep(50);
}

void Fila1(){
	int pos2=1;
	for(int a=1;a<=10;a++){
			int pos=1;
		for(int c=1;c<=13;c++){
	Cuadrado(pos,pos2);
	pos+=4;

	}
		pos2+=4;
	}
}

void Fila2(){
	int pos2=3;

	for(int a=1;a<=9;a++){
			int pos=3;
		for(int c=1;c<=13;c++){
	Cuadrado(pos,pos2);
	pos+=4;

	}
		pos2+=4;
	}
}

void Fila3(){
	int pos2=3;
	for(int a=1;a<=9;a++){
			int pos=1;
		for(int c=1;c<=13;c++){
	Cuadrado2(pos,pos2);
	pos+=4;

	}
		pos2+=4;
	}
}

void Fila4(){
	int pos2=1;

	for(int a=1;a<=10;a++){
			int pos=3;
		for(int c=1;c<=13;c++){
	Cuadrado2(pos,pos2);
	pos+=4;

	}
		pos2+=4;
	}
}

void Tablero(int x)
{
	Console::BackgroundColor::set(System::ConsoleColor::Black);
			Fila1();
			Fila2();
			Fila3();
			Fila4();
			Addon(x);
}
void Textos(){
	Console::ForegroundColor::set(System::ConsoleColor::White);
	Console::BackgroundColor::set(System::ConsoleColor::Black);
	Console::SetCursorPosition(63,1);
	printf("MAIN(F1)");
	Console::SetCursorPosition(61,14);
	printf("FUNCION A(F2)");
	Console::SetCursorPosition(61,24);
	printf("FUNCION B(F3)");
	Console::SetCursorPosition(55,34);
	Console::ForegroundColor::set(System::ConsoleColor::Magenta);
	printf("%c : Salta",126);
	Console::SetCursorPosition(55,35);
	Console::ForegroundColor::set(System::ConsoleColor::Blue);
	printf("%c : Avanza",24);
	Console::SetCursorPosition(55,36);
	Console::ForegroundColor::set(System::ConsoleColor::Red);
	printf("%c : Gira Izq",27);
	Console::SetCursorPosition(55,37);
	Console::ForegroundColor::set(System::ConsoleColor::DarkGreen);
	printf("%c : Gira Der",26);
	Console::SetCursorPosition(55,38);
	Console::ForegroundColor::set(System::ConsoleColor::Yellow);
	printf("%c : Enci/Apg",15);
		Console::SetCursorPosition(69,34);
	Console::ForegroundColor::set(System::ConsoleColor::Green);
	printf("A : FuncA");
		Console::SetCursorPosition(69,35);
		Console::ForegroundColor::set(System::ConsoleColor::Cyan);
	printf("B : FuncB");
		Console::SetCursorPosition(69,37);
		Console::ForegroundColor::set(System::ConsoleColor::White);
	printf("P : Play");

}
void Flechas(int x,int y)
{
Console::BackgroundColor=ConsoleColor::Black;
Console::ForegroundColor=ConsoleColor::Yellow;
Console::SetCursorPosition(x,y);
printf("%c",16);
Console::SetCursorPosition(x+2,y);
printf("%c",17);
}
void LimpiarFun(){
	Console::BackgroundColor::set(System::ConsoleColor::Black);
	for(int x=54;x<78;x++){
		for(int y=4;y<10;y++){
			Console::SetCursorPosition(x,y);
			printf(" ");
		}
		for(int y=16;y<21;y++){
			Console::SetCursorPosition(x,y);
			printf(" ");
		}
		for(int y=26;y<31;y++){
			Console::SetCursorPosition(x,y);
			printf(" ");
		}
	}
}
void Fondo(int n){
DibujaMarco();
Tablero(n);
Textos();
}

void Borrar(int P[]){
	Console::ForegroundColor::set(System::ConsoleColor::Black);
	Console::BackgroundColor::set(System::ConsoleColor::Black);
	int x=P[0];
	int y=P[1];
	Console::SetCursorPosition(x,y);
	printf(" ");
	Console::SetCursorPosition(x+2,y);
	printf(" ");
}

int Intercambioy(int x) {
	int posy;
	if(x==4 || x==16 || x==26)
		posy=1;
	if(x==6 || x==18 || x==28)
		posy=2;
	if(x==8 || x==20 || x==30)
		posy=3;
	if(x==10 || x==22 || x==32)
		posy=4;
	if(x==12 || x==24 || x==34)
		posy=5;
	posy--;
	return posy;
}

int Intercambiox(int x){
	int posx;
	if(x==54)
	posx=1;
	if(x==57)
	posx=2;
	if(x==60)
	posx=3;
	if(x==63)
	posx=4;
	if(x==66)
	posx=5;
	if(x==69)
	posx=6;
	if(x==72)
	posx=7;
	if(x==75)
	posx=8;
posx--;

	return posx;
}
void InsercionM(int P[], int M[][8])
{
	int f;
	int posx,posy;
	int x=P[0];
	int y=P[1];
	posx=Intercambiox(x);
	posy=Intercambioy(y);
		Flechas(x,y);
		Console::SetCursorPosition(x+1,y);
			do{f=_getch();
			if(f==TES)
				f=SUPRIM;
		if(f==224)
			f = _getch();
		M[posy][posx]=f;
	}while(f!=KEY_UP && f!=KEY_RIGHT && f!=KEY_LEFT && f!=SPACE && f!=SUPRIM && f!=TEB && f!=TEA && f!=ENTER);
		switch(f){
		case KEY_UP: 
			Console::ForegroundColor::set(System::ConsoleColor::Blue);
			printf("%c",24); break;
		case KEY_RIGHT: 
			Console::ForegroundColor::set(System::ConsoleColor::DarkGreen);
			printf("%c",26); break;
		case KEY_LEFT: 
			Console::ForegroundColor::set(System::ConsoleColor::Red);
			printf("%c",27); break;
		case SPACE: 
			Console::ForegroundColor::set(System::ConsoleColor::Yellow);
			printf("%c",15); break;
		case TEA: 
			Console::ForegroundColor::set(System::ConsoleColor::Green);
			printf("%c",65); break;
		case TEB: 
			Console::ForegroundColor::set(System::ConsoleColor::Cyan);
			printf("%c",66); break;
		case SUPRIM: 
			Console::ForegroundColor::set(System::ConsoleColor::Magenta);
			printf("%c",126); break;
		case ENTER: 
			Console::ForegroundColor::set(System::ConsoleColor::Black);
			printf(" "); break;
		}
		
}
void InsercionA(int P[], int M[][8])
{
	int f;
	int posx,posy;
	int x=P[0];
	int y=P[1];
	posx=Intercambiox(x);
	posy=Intercambioy(y);
		Flechas(x,y);
		Console::SetCursorPosition(x+1,y);
	do{f=_getch();
				if(f==TES)
				f=SUPRIM;
		if(f==224)
			f = _getch();
		M[posy][posx]=f;
	}while(f!=KEY_UP && f!=KEY_RIGHT && f!=KEY_LEFT && f!=SPACE && f!=SUPRIM && f!=TEB && f!=ENTER);
		switch(f){
		case KEY_UP: 
			Console::ForegroundColor::set(System::ConsoleColor::Blue);
			printf("%c",24); break;
		case KEY_RIGHT: 
			Console::ForegroundColor::set(System::ConsoleColor::DarkGreen);
			printf("%c",26); break;
		case KEY_LEFT: 
			Console::ForegroundColor::set(System::ConsoleColor::Red);
			printf("%c",27); break;
		case SPACE: 
			Console::ForegroundColor::set(System::ConsoleColor::Yellow);
			printf("%c",15); break;
		case TEB: 
			Console::ForegroundColor::set(System::ConsoleColor::Cyan);
			printf("%c",66); break;
		case SUPRIM: 
			Console::ForegroundColor::set(System::ConsoleColor::Magenta);
			printf("%c",126); break;
		case ENTER: 
			Console::ForegroundColor::set(System::ConsoleColor::Black);
			printf(" "); break;
		}
		
}
void InsercionB(int P[], int M[][8])
{
	int f;
	int posx,posy;
	int x=P[0];
	int y=P[1];
	posx=Intercambiox(x);
	posy=Intercambioy(y);
		Flechas(x,y);
		Console::SetCursorPosition(x+1,y);
			do{f=_getch();
						if(f==TES)
				f=SUPRIM;
		if(f==224)
			f = _getch();
		M[posy][posx]=f;
	}while(f!=KEY_UP && f!=KEY_RIGHT && f!=KEY_LEFT && f!=SPACE && f!=SUPRIM && f!=ENTER);
		switch(f){
		case KEY_UP: 
			Console::ForegroundColor::set(System::ConsoleColor::Blue);
			printf("%c",24); break;
		case KEY_RIGHT: 
			Console::ForegroundColor::set(System::ConsoleColor::DarkGreen);
			printf("%c",26); break;
		case KEY_LEFT: 
			Console::ForegroundColor::set(System::ConsoleColor::Red);
			printf("%c",27); break;
		case SPACE: 
			Console::ForegroundColor::set(System::ConsoleColor::Yellow);
			printf("%c",15); break;
		case TEA: 
			Console::ForegroundColor::set(System::ConsoleColor::Green);
			printf("%c",65); break;
		case SUPRIM: 
			Console::ForegroundColor::set(System::ConsoleColor::Magenta);
			printf("%c",126); break;
		case ENTER: 
			Console::ForegroundColor::set(System::ConsoleColor::Black);
			printf(" "); break;
		}
		
}
void Moverse(int c,int P[]){
	int x=P[0];
	int y=P[1];
	int lims,limi;
	if(y>=4 && y<=8)
	{lims=4;
	limi=12;
	}
	if(y>=16 && y<=19)
	{lims=16;
	limi=22;
	}
	if(y>=26 && y<=29)
	{lims=26;
	limi=32;
	}
	Console::SetCursorPosition(x,y);
	do{
		Flechas(x,y);
		c=_getch();
		if(c==224 || c==0)
			c=_getch();
		Borrar(P);	
		if(c==KEY_LEFT && x!=54)
			x-=3;
		if(c==KEY_UP && y!=lims)
			y-=2;
		if(c==KEY_RIGHT && x!=75)
			x+=3;
		if(c==KEY_DOWN && y!=limi)
			y+=2;
		if(c==F1 || c==F2 || c==F3){
			P[0]=54;
			if(c==F1){
			Borrar(P);
			P[1]=4;
			P[2]=1;
			Moverse(c,P);
			break;
			}
		if(c==F2){
			Borrar(P);
			P[1]=16;
			P[2]=2;
		    Moverse(c,P);
			break;
		}
		if(c==F3){
			Borrar(P);
			P[1]=26;
			P[2]=3;
			Moverse(c,P);
			break;
		}
	}
	if(c=='\r'){
		P[0]=x;
		P[1]=y;
		P[3]=1;
		break;
	}
	if(c==TEP){
		P[0]=x;
		P[1]=y;
		P[3]=2;
		break;
	}
	P[0]=x;
	P[1]=y;
	Console::SetCursorPosition(x,y);
	}while(c!=F1 || c!=F2 || c!=F3);
}
void Detecta(int c, int P[]){
	do{
		if(c==F1){
			Borrar(P);
			P[1]=4;
			P[2]=1;
			}
		if(c==F2){
			Borrar(P);
			P[1]=16;
			P[2]=2;
		}
		if(c==F3){
			Borrar(P);
			P[1]=26;
			P[2]=3;
		}
		if(c!=F1 && c!=F2 && c!=F3){
			c=_getch();
			if(c==0 || c==224)
				c=_getch();}
	}while(c!=59 && c!=60 && c!=61 || c==ENTER);
}	
void RobotJ(int f,int x, int y){
	Console::SetCursorPosition(x,y);
	Console::BackgroundColor::set(System::ConsoleColor::White);
	Console::ForegroundColor::set(System::ConsoleColor::Black);
	switch(f){
	case 1: 
		printf("O%c",16);
		Console::SetCursorPosition(x,y+1);
		printf("O%c",16);
		break;
	case 2:
		printf("OO");
		Console::SetCursorPosition(x,y+1);
		printf("%c%c",31,31);
		break;
	case 3:
		printf("%cO",17);
		Console::SetCursorPosition(x,y+1);
		printf("%cO",17);
		break;
	case 4:
		printf("%c%c",30,30);
		Console::SetCursorPosition(x,y+1);
		printf("OO");
		break;
	}
}

void RobotJ2(int f,int x, int y){
	Console::SetCursorPosition(x,y);
	Console::BackgroundColor::set(System::ConsoleColor::Yellow);
	Console::ForegroundColor::set(System::ConsoleColor::Black);
	switch(f){
	case 1: 
		printf("O%c",16);
		Console::SetCursorPosition(x,y+1);
		printf("O%c",16);
		break;
	case 2:
		printf("OO");
		Console::SetCursorPosition(x,y+1);
		printf("%c%c",31,31);
		break;
	case 3:
		printf("%cO",17);
		Console::SetCursorPosition(x,y+1);
		printf("%cO",17);
		break;
	case 4:
		printf("%c%c",30,30);
		Console::SetCursorPosition(x,y+1);
		printf("OO");
		break;
	}
}

void Meterseloalosarreglos(int M[][8], int A[][8], int B[][8], int M1[], int A1[], int B1[]){
	int a=0;
	for(int i=0;i<5;i++){
		for(int j=0;j<8;j++){
			if(M[i][j]!=0){
				M1[a]=M[i][j];
				a++;
			}
		}
	}
	a=0;
	for(int i=0;i<4;i++){
		for(int j=0;j<8;j++){
			if(M[i][j]!=0){
				A1[a]=A[i][j];
				a++;
			}
		}
	}
	a=0;
	for(int i=0;i<4;i++){
		for(int j=0;j<8;j++){
			if(M[i][j]!=0){
				B1[a]=B[i][j];
				a++;
			}
		}
	}
}

void Gana(int n,int x,int y,int r[]){

	switch(n){
		case 1:
			if(x==33 && y==21)
			r[0]=r[0]+1;
			Console::Beep(2000,30);
			_sleep(10);
			Console::Beep(2000,30);
			_sleep(10);
			Console::Beep(2000,30);
		case 2:
			if(x==25 && y==11)
			r[0]=r[0]+1;
			if(x==51 && y==1)
			r[0]=r[0]+1;
			Console::Beep(2000,30);
			_sleep(10);
			Console::Beep(2000,30);
			_sleep(10);
			Console::Beep(2000,30);
	}
}
void BorrarCondenado(int f, int x ,int y){
				if(f==1)
				{
					RobotJ(f,x,y);
					if((y-1)%4==0)
					{
						if((x-1)%4==0)
							Cuadrado2(x-2,y);
						else
							Cuadrado(x-2,y);
					}
					else
					{
						if((x-1)%4==0)
							Cuadrado(x-2,y);
						else
							Cuadrado2(x-2,y);
					}
				}
				if(f==2)
				{
					RobotJ(f,x,y);
					if((y-1)%4==0){
						if((x-1)%4==0)
							Cuadrado2(x,y-2);
						else
							Cuadrado(x,y-2);}
					else
					{
						if((x-1)%4==0)
							Cuadrado(x,y-2);
						else
							Cuadrado2(x,y-2);
					}
				}
					if(f==3)
					{
						RobotJ(f,x,y);
						if((y-1)%4==0)
						{
							if((x-1)%4==0)
								Cuadrado2(x+2,y);
							else
								Cuadrado(x+2,y);}
						else
						{
							if((x-1)%4==0)
								Cuadrado(x+2,y);
							else
								Cuadrado2(x+2,y);
						}
					}
					if(f==4)
					{
						RobotJ(f,x,y);
						if((y-1)%4==0)
						{
							if((x-1)%4==0)
								Cuadrado2(x,y+2);
							else
								Cuadrado(x,y+2);}
					else
					{
						if((x-1)%4==0)
							Cuadrado(x,y+2);
						else
							Cuadrado2(x,y+2);
					}
					}
}
void BorrarCondenado2(int f, int x ,int y){
				if(f==1)
				{
					RobotJ(f,x,y);
					if((y-1)%4==0)
					{
						if((x-1)%4==0)
							Cuadrado(x-4,y);
						else
							Cuadrado2(x-4,y);
					}
					else
					{
						if((x-1)%4==0)
							Cuadrado2(x-4,y);
						else
							Cuadrado(x-4,y);
					}
				}
				if(f==2)
				{
					RobotJ(f,x,y);
					if((y-1)%4==0){
						if((x-1)%4==0)
							Cuadrado(x,y-4);
						else
							Cuadrado2(x,y-4);}
					else
					{
						if((x-1)%4==0)
							Cuadrado2(x,y-4);
						else
							Cuadrado(x,y-4);
					}
				}
					if(f==3)
					{
						RobotJ(f,x,y);
						if((y-1)%4==0)
						{
							if((x-1)%4==0)
								Cuadrado(x+4,y);
							else
								Cuadrado2(x+4,y);}
						else
						{
							if((x-1)%4==0)
								Cuadrado2(x+4,y);
							else
								Cuadrado(x+4,y);
						}
					}
					if(f==4)
					{
						RobotJ(f,x,y);
						if((y-1)%4==0)
						{
							if((x-1)%4==0)
								Cuadrado(x,y+4);
							else
								Cuadrado2(x,y+4);}
					else
					{
						if((x-1)%4==0)
							Cuadrado2(x,y+4);
						else
							Cuadrado(x,y+4);
					}
					}
}
void FDF(int P[], int n,int f){
	int x=P[0];
	int y=P[1];

	switch(n){
		case 1:
			if(f==1 && x!=51)
			x+=2;
			if(f==2)
			y+=2;	
			if(f==3 && x!=1)
			x-=2;
			if(f==4 && y!=1)
			y-=2;
			BorrarCondenado(f,x,y);
			Console::Beep(37,10);
			_sleep(500);
			break;
		case 2:
			if(f==1 && x!=51){
				if(x==21 && y==11)
					Console::Beep(2000,10);
				else{
					if(x==39 && (y>=3 && y<=41))
						Console::Beep(2000,10);
					else
						x+=2;}
			}
			if(f==2 && y!=37){
				if(x==23 && y==9)
					Console::Beep(2000,10);
				else{
					if(x==41 && y==1)
						Console::Beep(2000,10);
					else
						y+=2;}
			}
			if(f==3 && x!=1){
				if(x==25 && y==11)
					Console::Beep(2000,10);
				else{
					if(x==43 && (y>=3 && y<=41))
						Console::Beep(2000,10);
					else
						x-=2;}	}
			if(f==4 && y!=1){
				if(x==23 && y==11)
					Console::Beep(2000,10);
				else{
					if(x==41 && y==43)
						Console::Beep(2000,10);
					else
						y-=2;}
			}			
			Console::Beep(37,10);
			BorrarCondenado(f,x,y);
			
			_sleep(500);
			break;
	}
	P[0]=x;
	P[1]=y;
}
void FDS(int P[], int n,int f){
	int x=P[0];
	int y=P[1];

	switch(n){
		case 1:
			Console::Beep(2000,10);
			_sleep(500);
			break;
		case 2:
			if(f==1){
				if(x==21 && y==11)
					x+=4;
				else	
					Console::Beep(2000,10);
			}
			if(f==2){
				if(x==23 && y==9)
					y+=4;
				else	
					Console::Beep(2000,10);
			}
			if(f==3){
				if(x==25 && y==11)
					x-=4;
				else	
					Console::Beep(2000,10);
			}
			if(f==4){
				if(x==23 && y==13)
					y-=4;
				else	
					Console::Beep(2000,20);
			}
			Console::Beep(37,10);
			BorrarCondenado2(f,x,y);
			_sleep(500);
			break;
	}
	P[0]=x;
	P[1]=y;
}
int MoverCondenado(int x, int y, int M[], int A[], int B[], int n){
	int i1=1;
	int f=1;
	int J=0;
	int R[1]={0};
	int P[2]={x,y};
	int cont=0;
	for(int i=0; i<=40; i++){
		switch(M[i]){
		case 0:
			goto fail;
					break;
		case KEY_UP:   if(n==2){
					if(x==25 && y==11)
						cont++;
					if(x==51 && y==1)
						cont++;
					}FDF(P,n,f);
					x=P[0];
					y=P[1];
					if(n==2){
						if(cont==1){
						if(R[0]==0){
							Foco(51,1);
							Foco(25,11);}

							if(R[0]==1)
							{
							FocoP(25,11);
							Foco(51,1);
							}
							if(R[0]==2){
							FocoP(51,1);
							FocoP(25,11);}
						cont=0;
						}
					}

				break;
				case KEY_RIGHT: 
					f++;
					if(f==5)
					f=1;
					RobotJ(f,x,y);
					_sleep(500);
					break;
				case KEY_LEFT: 
					f--;
					if(f==0)
					f=4;
					RobotJ(f,x,y);
					_sleep(500);
					break;
				case SPACE: 
					RobotJ2(f,x,y);
					Gana(n,x,y,R);
					_sleep(500);
					break;
				case SUPRIM:
				case TES:
					FDS(P,n,f);
					x=P[0];
					y=P[1];
					break;
				case TEA: 
					for(int j=0; j<32; j++){
							Console::BackgroundColor::set(System::ConsoleColor::Black);
							switch(A[j])
							{
								case 0:
									goto fail1;
									break;
								case KEY_UP:   if(n==2){
									if(x==25 && y==11)
						cont++;
					if(x==51 && y==1)
						cont++;
					}FDF(P,n,f);
					x=P[0];
					y=P[1];
					if(n==2){if(cont==1){
						if(R[0]==0){
							Foco(51,1);
							Foco(25,11);}
							if(R[0]==1){
							Foco(51,1);
							FocoP(25,11);}
							if(R[0]==2){
							FocoP(51,1);
							FocoP(25,11);}
							cont=0;}
					}
									break;
								case KEY_RIGHT: 
									f++;
									if(f==5)
									f=1;
									RobotJ(f,x,y);
									_sleep(500);
									break;
								case KEY_LEFT: 
									f--;
									if(f==0)
									f=4;
									RobotJ(f,x,y);
									_sleep(500);
									break;
								case SPACE: 
									RobotJ2(f,x,y);
									Gana(n,x,y,R);
									_sleep(500);
									break;
								case SUPRIM:
								case TES:
									FDS(P,n,f);
									x=P[0];
									y=P[1];
									break;
								case TEB: 
									for(int k=0; k<32; k++){
												Console::BackgroundColor::set(System::ConsoleColor::Black);
												switch(B[k])
												{
													case 0:
														goto fail2;
														break;
													case KEY_UP:   if(n==2){
														if(x==25 && y==11)
						cont++;
					if(x==51 && y==1)
						cont++;
					}FDF(P,n,f);
					x=P[0];
					y=P[1];
					if(n==2){
						if(cont==1){
							if(R[0]==0){
								Foco(51,1);
								Foco(25,11);}
								if(R[0]==1){
								Foco(51,1);
								FocoP(25,11);}
								if(R[0]==2){
								FocoP(51,1);
								FocoP(25,11);}
							cont=0;
						}}
														break;
													case KEY_RIGHT: 
														f++;
														if(f==5)
														f=1;
														RobotJ(f,x,y);
														_sleep(500);
														break;
													case KEY_LEFT: 
														f--;
														if(f==0)
														f=4;
														RobotJ(f,x,y);
														_sleep(500);
														break;
													case SPACE: 
														RobotJ2(f,x,y);
														Gana(n,x,y,R);
														_sleep(500);
														break;
																	case SUPRIM:
													case TES:
														FDS(P,n,f);
														x=P[0];
														y=P[1];
														break;
												}
									}fail2:0;
					}
					}fail1:0;
					break;
			case TEB: 
				for(int l=0; l<32; l++){
						Console::BackgroundColor::set(System::ConsoleColor::Black);
						switch(B[l])
						{
							case 0:
								goto fail3;
								break;
							case KEY_UP:   if(n==2){
								if(x==25 && y==11)
						cont++;
					if(x==51 && y==1)
						cont++;
					}FDF(P,n,f);
					x=P[0];
					y=P[1];
					if(n==2){if(cont==1){
						if(R[0]==0){
							Foco(51,1);
							Foco(25,11);}
							if(R[0]==1){
							Foco(51,1);
							FocoP(25,11);}
							if(R[0]==2){
							FocoP(51,1);
								FocoP(25,11);}
							cont=0;}
					}
								break;
							case KEY_RIGHT: 
								f++;
								if(f==5)
								f=1;
								RobotJ(f,x,y);
								_sleep(500);
								break;
							case KEY_LEFT: 
								f--;
								if(f==0)
								f=4;
								RobotJ(f,x,y);
								_sleep(500);
								break;
							case SPACE: 
								RobotJ2(f,x,y);
								Gana(n,x,y,R);
								_sleep(500);
								break;
							case SUPRIM:
							case TES:
								FDS(P,n,f);
								x=P[0];
								y=P[1];
								break;
						}
				}fail3:0;
				break;
			}
	}
fail:	Tablero(n);
	J=R[0];
return J;
}
void Ganaste(){
	Console::BackgroundColor::set(System::ConsoleColor::Black);
	int i=30;
	int j=2;
	char T[11]={'F','E','L','I','C','I','D','A','D','E','S'};
		Final();
	char T2[39]={'D','e','v','e','l','o','p','e','d',' ','b','y',' ','E','v','a',' ','R','o','m','e','r','o',' ','&',' ','D','a','v','i','d',' ','V','e','l','a','r','d','e'};
	Engendro1(34,38);
	Console::SetCursorPosition(38,34);
	printf("[O.O]");
	Console::ForegroundColor::set(System::ConsoleColor::Green);
	for(int y=0;y<11;y++){
		Console::SetCursorPosition(i,4);
		printf("%c\b\b",T[y]);
		_sleep(200);
		i+=2;
	}
	for(int y=0;y<39;y++){
		Console::SetCursorPosition(j,39);
		printf("%c\b\b",T2[y]);
		_sleep(200);
		j+=2;
	}
}
int main(array<System::String ^> ^args)
{
	int P[4]={0};
	P[2]=2;
	int x,y,n=1,g=0;
	Console::CursorVisible = false;
	Console::Title::set("Light Bot");
	Console::WindowHeight::set(40);
	int c;

	Intro();
	x=21;
	y=11;
		Fondo(n);
	do{
	int M[5][8]={0};
	int A[4][8]={0};
	int B[4][8]={0};
	int M1[40]={0};
	int A1[32]={0};
	int B1[32]={0};
	P[0]=54;
	P[1]=4;
	Tablero(n);
	RobotJ(1,x,y);
		c=_getch();
		if(c==0)
			c=_getch();
		Detecta(c,P);
			do{
				Moverse(c,P);
				if(P[3]==1)
				{
					Console::CursorVisible = true;
					if(P[2]==1)
						InsercionM(P,M);
					if(P[2]==2)
						InsercionA(P,A);
					if(P[2]==3)
						InsercionB(P,B);
					Console::CursorVisible = false;
				}
				if(P[3]==2){
			Meterseloalosarreglos(M,A,B,M1,A1,B1);
			g=MoverCondenado(x,y,M1,A1,B1,n);
			switch(n){
				case 1:
					if(g==1){
					n++;
					goto ganas;}
					break;
				case 2:
					if(g==2){
					n++;
					goto ganast;}
					break;
			}
			RobotJ(1,x,y);}
			}while(1);
	ganas:0;
	LimpiarFun();
	}while(1);
ganast:;
	Console::BackgroundColor::set(System::ConsoleColor::Black);
	Console::Clear();
	Ganaste();
	_getch();
	return 0;
}